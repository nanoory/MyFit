# MyFit - MyFitness
MyFit application is fitness app created to monitor the lifestyle.

## Table of content
* [About](#about)
* [Development plans](#development_plans)
* [TODO](#todo)
* [Features](#features)
* [Technologies](#technologies)
* [Supported OS and devices](#supported_os_and_devices)
* [Setup](#setup)

## About
MyFit is application dedicated for people who need fitness
app on their PinePhone but not only.
Generally Linux/BSD and Redox OS devices will be supported more.

## Development plans

## TODO
- Repair bug with GUI

## Features

## Technologies
- Rust
- Python 3

## Supported OS and devices

## Setup
