use orbtk::prelude::*;

// Langs
static PL_PL: &str = include_str!("../assets/lang/pl_PL.ron");
static DE_DE: &str = include_str!("../assets/lang/de_DE.ron");
static NO_NO: &str = include_str!("../assets/lang/no_NO.ron");


fn main() {
    println!("Hi in app MyFit");

    let localization = RonLocalization::create()
        .language("en_US")
        .dictionary("pl_PL", PL_PL)
        .dictionary("de_DE", DE_DE)
        .dictionary("no_NO", NO_NO)
        .build();

    Application::new()
    .localization(localization)
    .window(|ctx| {
        Window::new()
        .title("MyFit - MyFitness")
        .position((100.0, 100.0))
        .size(800.0, 650.0)
        .resizeable(true)
        .child(MainView::new().build(ctx))
        .build(ctx)
    })
    .run();
}

widget!(MainView {});
impl Template for MainView {
    fn template(self, _id: Entity, ctx: &mut BuildContext) -> Self {
        self.child(
            TabWidget::new()
                .tab("Welcome", WelcomeView::new().build(ctx))
                .tab("Add data", AddView::new().build(ctx))
                .tab("Show data", ShowView::new().build(ctx))
                .tab("Config", ConfigView::new().build(ctx))
                .build(ctx),
        )
    }
}

type List = Vec<String>;

#[derive(Debug, Default, AsAny)]
struct LocalizationState {
    change_language: bool,
}

impl LocalizationState {
    fn change_language(&mut self) {
        self.change_language = true;
    }
}

impl State for LocalizationState {
    fn update(&mut self, _registry: &mut Registry, ctx: &mut Context) {
        if !self.change_language {
            return;
        }

        let index = *WelcomeView::selected_index_ref(&ctx.widget()) as usize;
        let selected_language = WelcomeView::languages_ref(&ctx.widget())[index].clone();

        match selected_language.as_str() {
            "Polish" => ctx.set_language("pl_PL"),
            "German" => ctx.set_language("de_DE"),
            "Norsk" => ctx.set_language("no_NO"),
            _ => (),
        }
        self.change_language = false;
    }
}

widget!(WelcomeView<LocalizationState> { languages: List, selected_index: i32 });
impl Template for WelcomeView {
    fn template(self, id: Entity, ctx: &mut BuildContext) -> Self {
        let languages = vec!["Polish".to_string(), "German".to_string(), "Norsk".to_string()];
        let count = languages.len();
        self.languages(languages).selected_index(0).child(
            Grid::new()
                .margin(16)
                .columns(Columns::create().push(140).push(32).push(140))
                .child(
                    Stack::new()
                        .spacing(8)
                        .child(
                            ImageWidget::new()
                                .name("Logo MyFit")
                                .image("")
                                .build(ctx),
                        )
                        .child(
                            Button::new()
                                .text("Hi dev")
                                .icon(material_icons_font::MD_CHECK)
                                .build(ctx),
                        )
                        .child(
                            Button::new()
                                .text("Open git website")
                                .on_enter(|_,_| {
                                    println!("Opened website")
                                })
                                .icon(material_icons_font::MD_360)
                                .build(ctx),
                        )
                        .build(ctx),
                )
                .child(
                    Stack::new()
                        .attach(Grid::column(2))
                        .spacing(8)
                        .child(
                            // Setting language
                            ComboBox::new()
                                .count(count)
                                .items_builder(move |bc, index| {
                                    let text = WelcomeView::languages_ref(&bc.get_widget(id))[index].clone();
                                    TextBlock::new().v_align("center").text(text).build(bc)
                                })
                                .on_changed("selected_index", move |states, _| {
                                    states.get_mut::<LocalizationState>(id).change_language();
                                    // Saving setting
                                    //states.send_message(InteractiveAction::SaveSettings, id)
                                })
                                .selected_index(id)
                                .build(ctx),
                        )
                        .child(
                            TextBlock::new()
                                .text("Welcome in MyFit - MyFitness")
                                .style("header")
                                .build(ctx),
                        )
                        .child(
                            TextBlock::new()
                                .text("This is app for tracking your fitness progress.")
                                .style("body")
                                .build(ctx),
                        )
                        .child(
                            TextBox::new()
                                .water_mark("Insert name")
                                .build(ctx),
                        )
                        .child(
                            TextBlock::new()
                                .text("Dont worry all data is stored localy on your device.")
                                .style("body")
                                .build(ctx),
                        )
                        .build(ctx),
                )
                .build(ctx),
        )
    }
}

widget!(AddView {});
impl Template for AddView {
    fn template(self, _id: Entity, ctx: &mut BuildContext) -> Self {
        self.child(
            Grid::new()
                .margin(16)
                .columns(Columns::create().push(140).push(32).push(140))
                .build(ctx),
        )
    }
}

widget!(ShowView {});
impl Template for ShowView {
    fn template(self, _id: Entity, ctx: &mut BuildContext) -> Self {
        self.child(
            Grid::new()
                .margin(16)
                .columns(Columns::create().push(140).push(32).push(140))
                .build(ctx),
        )
    }
}

widget!(ConfigView {});
impl Template for ConfigView {
    fn template(self, _id: Entity, ctx: &mut BuildContext) -> Self {
        self.child(
            Grid::new()
                .margin(16)
                .columns(Columns::create().push(140).push(32).push(140))
                .build(ctx),
        )
    }
}
